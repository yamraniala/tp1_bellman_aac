/**
 * @file bellman.cpp
 * @author Yassir MRANI ALAOUI
 * @brief  file that contain the implementation of functions declared in bellman.hpp
 * @date 2024-01-08
 * 
 */
#include <iostream>
#include <fstream>
#include "bellman.hpp"
#include <vector>


double const max_value = 200000;


/**
 * @brief Read values from a text file and stock it in a graph object
 * 
 * @param file_name name of file that contain the parameters of our graph, the first line contain the number of node,
 * then the following n line are in the following form (nombre_voisin voisin_1 poid_1 voisin_2 poid_2 ...) and so for all the nnode
 *
 * @param graph An object that models the graph as a successor (neighbor) matrix and a weight matrix.
 *
 */
void read_graph(string file_name, t_graph& graph)
{

    std::ifstream fichier(file_name);

    fichier >> graph.n;
    
    for (int i = 0; i < graph.n; i++)
    {
        fichier >> graph.successeur[i];

        for (int j = 0; j < graph.successeur[i]; j++)
        {
            fichier >> graph.s[i][j];
            fichier >> graph.l[i][j];
        }
    }
}



/**
 *
 * @brief Function that computes the shortest path from the graph and stores it in the solution
 *
 * @param graph Object modeling the graph as a successor (neighbor) matrix and a weight matrix
 *
 * @param solution Object describing the solution as an array storing the sequence of nodes representing the shortest path
 *
 */
void run_bellman(t_graph& graph, t_solution& solution)
{
    int stable = 1;

    std::cout << "\nUsing bellman algorithm : \n" << std::endl;

    for (int i = 0; i < graph.n; i++)
    {
        solution.pere[i] = -1;

        if(i != 0) solution.m[i] = max_value;
        else solution.m[i] = 0;
    }

    do
    {
        stable = 1;

        for (int i = 0; i < graph.n; i++)
        {
            for (int j = 0; j < graph.successeur[i]; j++)
            {

                int successeur = graph.s[i][j] - 1;
                
                if (solution.m[successeur] > solution.m[i] + graph.l[i][j])
                {
                    solution.m[successeur] = solution.m[i] + graph.l[i][j];
                    solution.pere[successeur] = i;
                 
                    stable = 0;
                }
            }
        }
    } while (stable == 0);
}



/**
 * @brief Displays the shortest path in the console from a source to a destination
 *
 * @param solution Object describing the solution as an array storing the sequence of nodes representing the shortest path
 *
 * @param from Source node
 *
 * @param to Destination node
 *
 */
void display_trajectory(t_solution& solution, int from, int to)
{
    int current_index = to - 1;
    vector<std::string> sol_str;

    try {
        while (current_index != from - 1)
        {
            sol_str.push_back(std::to_string(current_index + 1));
            current_index = solution.pere[current_index];
        }

        sol_str.push_back(std::to_string(current_index + 1));
        std::cout << "\t\t\tThe shortest path  from the node number  " << from << " to the node number "<< to << " is :  \n\n\t\t\t\t\t";

        for (int i = sol_str.size() - 1; i >= 0; i--)
        {

            std::cout << sol_str.at(i);

            if (i != 0) cout << ", ";
        }

        std::cout << endl << endl;
    }
    catch (std::string s)
    {

    }
}