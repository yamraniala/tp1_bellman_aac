/**
 * @file main_bellman.cpp
 * @author Yassir MRANI ALAOUI
 * @brief Entry point
 * @date 2024-01-08
 * 
 */
#include <iostream>
#include <fstream>
#include "bellman.hpp"


// int main()
// {
//     t_graph graph;
// 	t_solution solution;

// 	read_graph("test_file.txt", graph);

//     run_bellman(graph, solution);

//     display_trajectory(solution, 3, 9);

// 	return 0;
// }

