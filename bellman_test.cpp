/**
 * @file bellman.cpp
 * @author Yassir MRANI ALAOUI
 * @brief File that contain some test
 * @date 2024-01-08
 * 
 * @copyright Copyright (c) 2024
 * 
 */
#include "catch.hpp"

#include <sstream>
#include <iostream>
#include <type_traits>

#include "bellman.hpp"

TEST_CASE("Exection of Bellman's Algorithme: FROM 1 to 7")
{
    t_graph graph;
	t_solution solution;
	int from=1, to =7; 
    int current_index = to - 1;
    vector<std::string> sol_str;
	
	read_graph("test_file.txt", graph);

    run_bellman(graph, solution);

    current_index = to - 1;

    while (current_index != from - 1)
    {
        sol_str.push_back(std::to_string(current_index + 1));
        current_index = solution.pere[current_index];
    }

    sol_str.push_back(std::to_string(current_index + 1));

    REQUIRE ( sol_str[0] == "7" );
    REQUIRE ( sol_str[1] == "6" );
    REQUIRE ( sol_str[2] == "3" );
    REQUIRE ( sol_str[3] == "2" );
    REQUIRE ( sol_str[4] == "1" );
 
}

TEST_CASE("Exection of Bellman's Algorithme: FROM 3 TO 9")
{


    t_graph graph;
	t_solution solution;
	int from=3, to = 9;
    int current_index = to - 1;
    vector<std::string> sol_str;
	
	read_graph("test_file.txt", graph);

    run_bellman(graph, solution);

    current_index = to - 1;
    
    while (current_index != from - 1)
    {
        sol_str.push_back(std::to_string(current_index + 1));
        current_index = solution.pere[current_index];
    }

    sol_str.push_back(std::to_string(current_index + 1));

    REQUIRE ( sol_str[0] == "9" );
    REQUIRE ( sol_str[1] == "8" );
 
}