/**
 * @file bellman.hpp
 * @author Yassir MRANI ALAOUI
 * @brief file thait contains different declarations of functions and srtuctures used in our program
 * @date 2024-01-08
 * 
 * @copyright Copyright (c) 2024
 * 
 */

#ifndef BELLMAN_H
#define BELLMAN_H

#define n_max 1000
#define n_succeseur 1000

#include <string>


using namespace std;



/**
 * @brief Structure representing a graph.
 * 
 * This structure encapsulates information about a graph, including the number of vertices,
 * successor and weight matrices, and the number of successors for each node.
 */
typedef struct t_graph
{
    int n; /**< Number of vertices in the graph */
    int s[n_max][n_succeseur]; /**< Successor matrix of vertices */
    float l[n_max][n_succeseur]; /**< Weight matrix of graph edges */
    int successeur[n_max]; /**< Array storing the number of successors for each node */
} t_graph;



/**
 * @brief Structure representing a solution.
 * 
 * This structure encapsulates information about a solution, including the minimum distances
 * and the parent nodes for the shortest path.
 */
typedef struct t_solution {
    float m[n_max]; /**< Array storing minimum distances */
    int pere[n_max]; /**< Array storing parent nodes for the shortest path */
} t_solution;



/**
 * @brief Read values from a text file and stock it in a graph object
 * 
 * @param file_name name of file that contain the parameters of our graph, the first line contain the number of node,
 * then the following n line are in the ffollowing form (nombre_voisin voisin_1 poid_1 voisin_2 poid_2 ...) and so for all the nnode
 * @param graph An object that models the graph as a successor (neighbor) matrix and a weight matrix.
 *
 */
void read_graph(string file_name, t_graph& graph);



/**
 *
 * @brief Function that computes the shortest path from the graph and stores it in the solution
 *
 * @param graph Object modeling the graph as a successor (neighbor) matrix and a weight matrix
 *
 * @param solution Object describing the solution as an array storing the sequence of nodes representing the shortest path
 *
 */
void run_bellman(t_graph& graph, t_solution& solution);



/**
 * @brief Displays the shortest path in the console from a source to a destination
 *
 * @param solution Object describing the solution as an array storing the sequence of nodes representing the shortest path
 *
 * @param from Source node
 *
 * @param to Destination node
 *
 */
void display_trajectory(t_solution& solution, int from, int to);


#endif